/*   libfar
 *   Copyright (C) 2021  UMI Y
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <Error.hpp>

#define error_impl(err, name) name(const char* str) : err(str) { } \
                              name(const std::string& str) : err(str) { }

error_impl(std::runtime_error, std::null_pointer_error::null_pointer_error)
error_impl(std::runtime_error, std::file_not_found_error::file_not_found_error)
error_impl(std::runtime_error, std::size_zero_error::size_zero_error)
error_impl(std::runtime_error, std::xml_unknown_element_error::xml_unknown_element_error)
error_impl(std::runtime_error, std::xml_parse_error::xml_parse_error)
error_impl(std::runtime_error, std::invalid_entry_error::invalid_entry_error)
