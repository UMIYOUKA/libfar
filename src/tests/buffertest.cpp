/*   libfar
 *   Copyright (C) 2021  UMI Y
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <Buffer.hpp>
#include <Log.hpp>

#define T_SUCCESS 0
#define T_SKIP 77
#define T_FAILURE(CODE) CODE

#define M_FAIL "Buffer test failed! "
#define C_TEST(CODE, CONDITION, MESSAGE) if(CONDITION) { Log(M_FAIL<<MESSAGE, LOG_ERROR); return T_FAILURE(CODE); }

struct some_struct {
    int data        = 0;
    float dataf     = 0;
    std::string why = "Why";
};

class some_class {
public:
    int data_member = 0;

    some_class() {
        data_member = 50;
    }
};

int main(int argc, char *argv[]) {
    Buffer<std::byte> b_test;
    C_TEST(1, b_test.get_ptr() || b_test.get_size(), "Buffer should have been empty and null!");
    
    b_test = Buffer<std::byte>(60);
    C_TEST(2, !b_test.get_ptr() || b_test.get_size() != 60, "Buffer size invalid, or pointer is null!");

    b_test = Buffer<std::byte>(b_test);
    C_TEST(3, !b_test.get_ptr() || b_test.get_size() != 60, "Buffer copy size failed!");

    Buffer<some_struct> s_test(1);
    C_TEST(4, !s_test.get_ptr() || s_test.get_raw_size() != sizeof(some_struct), "Buffer and struct size mismatch!");

    Buffer<some_class> c_test(2);
    C_TEST(5, !c_test.get_ptr() || c_test[0].data_member != 50 || c_test[1].data_member != 50, "Class initialization in buffer error!");

    return T_SUCCESS;
}
