/*   libfar
 *   Copyright (C) 2021  UMI Y
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <MessageBox.hpp>
#include <Log.hpp>

#define T_SUCCESS 0
#define T_SKIP 77
#define T_FAILURE(CODE) CODE

#define M_FAIL "Buffer test failed! "
#define C_TEST(CODE, CONDITION, MESSAGE) if(CONDITION) { Log(M_FAIL<<MESSAGE, LOG_ERROR); return T_FAILURE(CODE); }


int main(int argc, char *argv[]) {
    MessageBox mb_test;

    mb_test.add_button(MessageBoxButton());
    mb_test.add_button(MessageBoxButton("Press me", []() -> void{
        Log("Button has been pressed");
    }));

    mb_test.open();

    return T_SUCCESS;
}
