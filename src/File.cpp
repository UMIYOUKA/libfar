/*   libfar
 *   Copyright (C) 2021  UMI Y
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <File.hpp>

size_t File::get_size() {
    seekg(0, end);
    size_t s_size = tellg();
    seekg(0, beg);

    return s_size;
}

void File::read_block(Buffer<std::byte>& ab_out, size_t as_start, size_t as_size) {
    if(as_size == 0 || beg + as_start > end) {
        ab_out.set_data(0);
        return; }

    ab_out.set_data(as_size);

    seekg(as_start, beg);
    read((char*)ab_out.get_ptr().get(), as_size);
}

Buffer<std::byte> File::read_block(size_t as_start, size_t as_size) {
    Buffer<std::byte> b_out;
    read_block(b_out, as_start, as_size);
    return b_out;
}
