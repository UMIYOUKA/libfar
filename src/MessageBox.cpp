/*   libfar
 *   Copyright (C) 2021  UMI Y
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <MessageBox.hpp>
#include <Log.hpp>

#include <cstdlib>

#include <Xm/Xm.h>
#include <Xm/PushB.h>

//

MessageBoxButton::MessageBoxButton(const std::string& as_label,
                                   std::function<void ()> af_callback) {
    label    = as_label;
    callback = af_callback; }

//

void btn_callback(_WidgetRec*, void* u_data, void*) {
    MessageBoxButton* btn = (MessageBoxButton*)u_data;
    if(btn && btn->callback) {
        btn->callback();
    }
}

void MessageBox::add_button(const MessageBoxButton& amb_button) {
    buttons.push_back(amb_button);
}

void MessageBox::open() {
    Widget w_toplevel;
    std::vector<Widget> w_buttons;
    XtAppContext x_app;

    int v_argv = 0;
    char** v_argc = (char**)malloc(1);

    w_toplevel = XtVaAppInitialize(&x_app, title.c_str(), NULL, 0, &v_argv, v_argc, NULL, NULL);

    free(v_argc);

    for(auto& f_b : buttons) {
        char* lbl_out = (char*)malloc(f_b.label.size() + 1);
        memcpy(lbl_out, f_b.label.c_str(), f_b.label.size() + 1);
        Widget btn = XmCreatePushButton(w_toplevel, lbl_out, NULL, 0);
        XtManageChild(btn);

        XtAddCallback(btn, XmNactivateCallback, &btn_callback, &f_b);
    }

    XtRealizeWidget(w_toplevel);
    XtAppMainLoop(x_app);
}
