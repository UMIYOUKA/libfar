/*   libfar
 *   Copyright (C) 2021  UMI Y
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>
#include "Buffer"

template<std::size_t NUM_CHILDREN, typename LEAF_TYPE>
class FixedTree : LEAF_TYPE {
protected:
    Buffer<FixedTree<NUM_CHILDREN, LEAF_TYPE>> b_leaves = Buffer<FixedTree<NUM_CHILDREN, LEAF_TYPE>>();

public:
    void split() {
        b_leaves.set_data(NUM_CHILDREN);
    }

    bool has_split() const {
        return b_leaves.get_size() > 0;
    }

    Buffer<FixedTree<NUM_CHILDREN, LEAF_TYPE>>&       get_leaves()       { return b_leaves; }
    Buffer<FixedTree<NUM_CHILDREN, LEAF_TYPE>> const& get_leaves() const { return b_leaves; }

    // CONSTRUCTORS AND DESTRUCTORS //

    FixedTree(bool a_split=false) {
        if(a_split)
            split();
    }

    virtual ~FixedTree() { }

    // OPERATORS //

    LEAF_TYPE const& operator[](size_t pos) const { return b_leaves[pos]; }
          LEAF_TYPE& operator[](size_t pos)       { return b_leaves[pos]; }
};
