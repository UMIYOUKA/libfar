/*   libfar
 *   Copyright (C) 2021  UMI Y
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ostream>

template<typename T>
class Type2D {
public:
  T x,y;

  Type2D() { }
  Type2D(const T& A) : x(A), y(A) { }
  Type2D(const T& X,const T& Y) : x(X), y(Y) { }
  Type2D(const Type2D& p)          : x(p.x), y(p.y) { }
  template<typename D>
  Type2D(const Type2D<D>& p) : x(p.x), y(p.y) { }

  const Type2D operator+(const Type2D& p) const { return Type2D<T>(x+p.x,y+p.y); }
  const Type2D operator-(const Type2D& p) const { return Type2D<T>(x-p.x,y-p.y); }
  const Type2D operator*(const Type2D& p) const { return Type2D<T>(x*p.x,y*p.y); }
  const Type2D operator/(const Type2D& p) const { return Type2D<T>(x/p.x,y/p.y); }

  const Type2D operator*(const T& v) const { return Type2D<T>(x*v,y*v); }
  const Type2D operator/(const T& v) const { return Type2D<T>(x/v,y/v); }

  const Type2D operator-() const { return Type2D<T>(-x, -y); }

  Type2D& operator=(const Type2D& p) { x=p.x;y=p.y; return *this; }

  Type2D& operator+=(const Type2D& p) { return (*this)=Type2D(x+p.x,y+p.y); }
  Type2D& operator-=(const Type2D& p) { return (*this)=Type2D(x-p.x,y-p.y); }
  Type2D& operator*=(const Type2D& p) { return (*this)=Type2D(x*p.x,y*p.y); }
  Type2D& operator/=(const Type2D& p) { return (*this)=Type2D(x/p.x,y/p.y); }

  Type2D& operator*=(const T& v) { return (*this)=Type2D(x*v,y*v); }
  Type2D& operator/=(const T& v) { return (*this)=Type2D(x/v,y/v); }
};

template<typename T> bool operator==(const Type2D<T>& a,const Type2D<T>& b) {
  return (a.x==b.x)&&(a.y==b.y);
}

template<typename T> bool operator!=(const Type2D<T>& a,const Type2D<T>& b) {
  return !(a==b);
}

template<typename T> std::ostream& operator<<(std::ostream& str, const Type2D<T>& u) {
  str<<"("<<u.x<<","<<u.y<<")";
  return str;
}



template<typename T>
class Type3D {
public:
  T x,y,z;

  Type2D<T> xy() const { return Type2D<T>(x,y); }
  Type2D<T> yz() const { return Type2D<T>(y,z); }

  Type3D() { }
  Type3D(const T& A) : x(A), y(A), z(A) { }
  Type3D(const T& X,const T& Y,const T& Z)
    : x(X  ), y(Y  ), z(Z  ) { }

  Type3D(const T& X,const Type2D<T>& E)
    : x(X  ), y(E.x), z(E.y) { }

  Type3D(const Type2D<T>& B,const T& Z)
    : x(B.x), y(B.y), z(Z  ) { }

  Type3D(const Type3D& p)
    : x(p.x), y(p.y), z(p.z) { }

  const Type3D operator+(const Type3D& p) const { return Type3D<T>(x+p.x,y+p.y,z+p.z); }
  const Type3D operator-(const Type3D& p) const { return Type3D<T>(x-p.x,y-p.y,z-p.z); }
  const Type3D operator*(const Type3D& p) const { return Type3D<T>(x*p.x,y*p.y,z*p.z); }
  const Type3D operator/(const Type3D& p) const { return Type3D<T>(x/p.x,y/p.y,z/p.z); }

  const Type3D operator*(const T& v) const { return Type3D<T>(x*v,y*v,z*v); }
  const Type3D operator/(const T& v) const { return Type3D<T>(x/v,y/v,z/v); }

  const Type3D operator-() const { return Type3D<T>(-x, -y, -z); }

  Type3D& operator=(const Type3D& p) { x=p.x;y=p.y;z=p.z; return *this; }

  Type3D& operator+=(const Type3D& p) { return (*this)=Type3D(x+p.x,y+p.y,z+p.z); }
  Type3D& operator-=(const Type3D& p) { return (*this)=Type3D(x-p.x,y-p.y,z-p.z); }
  Type3D& operator*=(const Type3D& p) { return (*this)=Type3D(x*p.x,y*p.y,z*p.z); }
  Type3D& operator/=(const Type3D& p) { return (*this)=Type3D(x/p.x,y/p.y,z/p.z); }

  Type3D& operator*=(const T& v) { return (*this)=Type3D(x*v,y*v,z*v); }
  Type3D& operator/=(const T& v) { return (*this)=Type3D(x/v,y/v,z/v); }
};

template<typename T> bool operator==(const Type3D<T>& a,const Type3D<T>& b) {
  return (a.x==b.x)&&(a.y==b.y)&&(a.z==b.z);
}

template<typename T> bool operator!=(const Type3D<T>& a,const Type3D<T>& b) {
  return !(a==b);
}

template<typename T> std::ostream& operator<<(std::ostream& str, const Type3D<T>& u) {
    str<<"("<<u.x<<","<<u.y<<","<<u.z<<")";
    return str;
}

// Define common 3D and 2D point types //

using int3       = Type3D<int>;
using long3      = Type3D<long>;
using longlong3  = Type3D<long long>;

using uint3      = Type3D<unsigned int>;
using ulong3     = Type3D<unsigned long>;
using ulonglong3 = Type3D<unsigned long long>;

using float3     = Type3D<float>;
using double3    = Type3D<double>;

using size3  = int3;
using point3 = int3;

using int2       = Type2D<int>;
using long2      = Type2D<long>;
using longlong2  = Type2D<long long>;

using uint2      = Type2D<unsigned int>;
using ulong2     = Type2D<unsigned long>;
using ulonglong2 = Type2D<unsigned long long>;

using float2     = Type2D<float>;
using double2    = Type2D<double>;

using size2i  = int2;
using point2i = int2;

using size2f  = float2;
using point2f = float2;
