#pragma once

#include "FixedTree.hpp"

template<typename T>
class BinaryTree : FixedTree<2, T> {
public:
    BinaryTree(bool a_split=false) : FixedTree(a_split) { }

    virtual ~BinaryTree() { }
}
