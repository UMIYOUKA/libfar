#pragma once

#include <stdexcept>

/*
 * Error.hpp
 * 
 * Creates more usable runtime errors that could fit into a
 * larger category of issues which can help specify crashes
 * easier. All are solely based off of std::runtime_error()
 */

#define err_def(err, name) class name : public err { public: name(const char*); name(const std::string&); };

namespace std {
    err_def(std::runtime_error, null_pointer_error)
    err_def(std::runtime_error, file_not_found_error)
    err_def(std::runtime_error, size_zero_error)
    err_def(std::runtime_error, xml_unknown_element_error)
    err_def(std::runtime_error, xml_parse_error)
    err_def(std::runtime_error, invalid_entry_error)
}

#undef err_def
