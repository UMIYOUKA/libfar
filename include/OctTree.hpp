#pragma once

#include "FixedTree.hpp"

template<typename T>
class OctTree : FixedTree<8, T> {
public:
    OctTree(bool a_split=false) : FixedTree(a_split) { }

    virtual ~OctTree() { }
}
