#pragma once

#include <functional>
#include <vector>
#include <string>

/*
 * Simple Cross-Platform message box system for error reporting /
 * debugging purposes. Not on windows yet because... none of my
 * code has ever been tested on windows yet.
 */

class MessageBoxButton {
public:
    std::string            label = "Button";
    std::function<void ()> callback;

    MessageBoxButton(const std::string& as_label = "Button",
                     std::function<void()> af_callback = []() -> void{});
};

class MessageBox {
public:
    std::string title = "Message Box";
    std::string text = "Contents";

    std::vector<MessageBoxButton> buttons;

    void open();
    void add_button(const MessageBoxButton&);
};
