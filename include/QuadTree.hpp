#pragma once

#include "FixedTree.hpp"

template<typename T>
class QuadTree : FixedTree<4, T> {
public:
    QuadTree(bool a_split=false) : FixedTree(a_split) { }

    virtual ~QuadTree() { }
}
