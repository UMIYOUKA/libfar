#pragma once

#include <fstream>
#include "Buffer.hpp"

/*
 * File
 * 
 * A skeleton of a class that will be expanded to include more
 * useful functions to reduce code in other areas that need it.
 */

class File : public std::fstream {
public:
    using std::fstream::fstream;

    size_t get_size();
    Buffer<std::byte> read_block(size_t as_start, size_t as_size);
    void              read_block(Buffer<std::byte>& ab_out, size_t as_start, size_t as_size);
};
