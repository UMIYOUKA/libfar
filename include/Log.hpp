/*   libfar
 *   Copyright (C) 2021  UMI Y
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

// Really Ugly Logging Functions //
// easy to call :
// log("message")
// log("message", severity)
// Can even use << in the message param
// log("message ["<<0<<"]")

#include <iostream>
#include <iomanip>
#include <string.h>

enum __log_debug_type {
  LOG_WARNING ,
  LOG_ERROR   ,
  LOG_CRITICAL,
  LOG_FATAL
};

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#define __log_head \
"\e[0;1m│\e[32m"<<__FILENAME__<<"\e[0;1m \e[34m"<<__PRETTY_FUNCTION__ \
<<"\e[0;1m at \e[33m"<<std::setfill('0')<<std::setw(4)<<__LINE__

#define __log_head_end \
"\e[0;1m│\e[0m "

#define __log_nosev(msg) std::cout<<__log_head \
<<__log_head_end<<msg<<"\n"

#define __log_warning(msg)  std::cout<<__log_head<<"\e[0;1m┆\e[30;43m WARNING " \
<<__log_head_end<<msg<<"\n"

#define __log_error(msg)  std::cout<<__log_head<<"\e[0;1m┆\e[30;41m ERROR " \
<<__log_head_end<<msg<<"\n"

#define __log_critical(msg)  std::cout<<__log_head<<"\e[0;1m┆\e[30;46m CRITICAL " \
<<__log_head_end<<msg<<"\n"

#define __log_fatal(msg)  std::cout<<__log_head<<"\e[0;1m┆\e[31;43m FATAL " \
<<__log_head_end<<msg<<"\n"

#define __log_severity(msg, sev) \
static_assert(sev == LOG_WARNING  || sev == LOG_ERROR || \
                sev == LOG_CRITICAL || sev == LOG_FATAL, \
                "Log severity not of enum __log_debug_type"); \
switch(sev) { \
case LOG_WARNING : __log_warning (msg); break;\
case LOG_ERROR   : __log_error   (msg); break;\
case LOG_CRITICAL: __log_critical(msg); break;\
case LOG_FATAL   : __log_fatal   (msg); break;\
default:\
  __log_nosev(msg); \
}

#define __log_macro_3rd_arg(arg1, arg2, arg3, ...) arg3
#define __log_macro_chooser(...) \
        __log_macro_3rd_arg(__VA_ARGS__, __log_severity, __log_nosev,)

#define Log(...) __log_macro_chooser(__VA_ARGS__)(__VA_ARGS__)

#ifdef DEBUG
#define DLog(...) Log("DEBUG │ "<<__VA_ARGS__)
#else
#define DLog(...)
#endif
