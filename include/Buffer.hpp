/*   libfar
 *   Copyright (C) 2021  UMI Y
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "Error.hpp"

#include <memory>
#include <cstring>
#include <fstream>
#include <initializer_list>

/*
 * Literal to help make std::byte a little easier to use
 */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wnarrowing"

constexpr std::byte operator "" _byte(unsigned long long int a_l) {
    return std::byte{a_l}; }

#pragma GCC diagnostic pop

/*
 * Buffer
 * 
 * Essentially a replacement for manually creating buffers
 * as it serves the same purpose but it can be called with
 * a smaller amount of code.
 */

template<typename T>
class Buffer {
protected:
    std::shared_ptr<T> _t_data = nullptr;
    uint32_t _u_size = 0;

public:
    Buffer()
        { }

    Buffer(size_t as_size)
        { set_data(as_size); }

    Buffer(const T* acc_data, size_t as_size)
        { set_data(acc_data, as_size); }

    Buffer(const std::initializer_list<T>& il_data)
        { set_data(il_data); }

    Buffer(const Buffer<T>& ab_src)
        { set_data(ab_src); }

    //

    void set_data(size_t as_size) {
        if(!as_size) {
            _u_size = 0;
            _t_data = nullptr;
            return; }

        _u_size = as_size;
        //_t_data = std::shared_ptr<T>((T*)operator new(_u_size * sizeof(T)));
        _t_data = std::shared_ptr<T>(new T[_u_size], [](T* a_t) { if(a_t) { delete[] (a_t); } });
    }

    void set_data(const T* acc_data, size_t as_size) {
        if(!acc_data) {
            set_data(0);
            return; }

        set_data(as_size);
        std::memcpy(get_ptr().get(), acc_data, as_size * sizeof(T));
    }

    void set_data(const std::shared_ptr<T>& acp_data, size_t as_size) {
        set_data(acp_data.get(), as_size);
    }

    void set_data(const Buffer<T>& ab_src) {
        set_data(ab_src.get_ptr(), ab_src.get_size());
    }

    void set_data(const std::initializer_list<T>& il_data) {
        if(il_data.size() == 0) {
            set_data(0);
            return; }

        set_data(il_data.size());
        std::memcpy(get_ptr().get(), il_data.begin(), get_size() * sizeof(T));
    }

    //

    const std::shared_ptr<T>& get_ptr() const { return _t_data; }
          std::shared_ptr<T>& get_ptr()       { return _t_data; }

    T const& operator[](size_t pos) const { return _t_data.get()[pos]; }
          T& operator[](size_t pos)       { return _t_data.get()[pos]; }

    void operator =(const Buffer<T>& a_buf) {
        set_data(a_buf);
    }

    size_t get_size() const { return _u_size; }
    size_t get_raw_size() const { return _u_size * sizeof(T); }

    virtual ~Buffer() { }
};
