# FAR
### A project to make C++ not as annoying
##### In my opinion

# Features
## Buffer\<T\>
An array-esc buffer class used for creating and deleting chunks of memory in a semi-controlled way. It was added to help keep the whole temporary buffer situation under control when handling multiple file loading libraries. This class is very much an array that can be reset to a new size, like some other STL class that I totally forgot existed when I made it, but now I'm stuck with ```Buffer<T>```

## Log(message, level[optional])
A useful debugging tool that displays the file and line number, alongside a message. Multiple levels can be called, including ```LOG_INFO```, ```LOG_WARNING```, ```LOG_ERROR```, and ```LOG_CRITICAL```.

A debug specific variant exists under the name ```DLog```, which is only enabled if a compile time variable DEBUG is defined. It is used like std::cout messages are, with a lot of << shifting nonsense, but that just happens to be the type of syntax I like :>
```
Log("Give me"<<" The "<<"Message"); // End lines are automatically appended
Log("Oops", LOG_ERROR);
```

## Units
A collection of 2D and 3D units based around the standard types. I don't know why languages don't
just provide these since they are used pretty much everywhere but \\_(-v-)_/

You can define a new dimensional unit by doing
```
using new_unit2D = Type2D<cool_type>;
using new_unit3D = Type3D<cooler_type>;
```

## File
A simple wrapper around ```std::fstream```, it provides a function for reading directly into a ```Buffer<std::byte>```,
alongside a function that provides the files size ```get_size()```

## MessageBox
Currently not fully implemented, MessageBox is supposed to be a platform agnostic way of display an
```Oh No I Fucked Up``` Message to the user, without crashing your program, or causing a huge ruckus.

## FixedTree\<T,N\>
Provides a simple way to make binary, ternary, quad, and N child trees with a template and nothing else.
I've never actually tested it since the code this is used for hasn't been created yet, so have fun

